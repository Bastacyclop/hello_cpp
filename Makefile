SRCDIR := src
BLDDIR := build
FLAGS := -Wall -Wextra -Werror -pedantic -pedantic-errors -std=c++14

all: $(BLDDIR) $(BLDDIR)/main

.PHONY: run
run: $(BLDDIR) $(BLDDIR)/main
	@$(BLDDIR)/main

MAIN_DEPS := $(BLDDIR)/other.o

$(BLDDIR)/main: $(MAIN_DEPS) $(SRCDIR)/main.cpp
	g++ $(FLAGS) $(MAIN_DEPS) $(SRCDIR)/main.cpp -o $(BLDDIR)/main

$(BLDDIR)/%.o: $(SRCDIR)/%.cpp $(SRCDIR)/%.hpp
	@echo "Compiling $< into $@"
	@g++ -c $(FLAGS) $< -o $@

$(BLDDIR):
	@mkdir -p $(BLDDIR)

.PHONY: clean
clean:
	rm -rf $(BLDDIR)
